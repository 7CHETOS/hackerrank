package com.hackerrank.java.medium;

import java.util.Scanner;
import java.util.Stack;

public class JavaStack {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    while (sc.hasNext()) {
      String input = sc.next();
      // Complete the code
      String[] lines = input.split("");
      System.out.println(isBalanced(lines));
    }
  }

  private static boolean isBalanced(String[] input) {

    Stack<String> stack = new Stack<String>();
    for (int i = 0; i < input.length; i++) {

      String aperture1 = "(";
      String aperture2 = "[";
      String aperture3 = "{";

      String cierre1 = ")";
      String cierre2 = "]";
      String cierre3 = "}";

      if (aperture1.equals(input[i]) || aperture2.equals(input[i]) || aperture3.equals(input[i])) {
        stack.push(input[i]);

      } else if (!stack.empty()) {
        switch (stack.peek()) {
          case "(":
            if (input[i].equals(cierre1)) stack.pop();
            break;
          case "[":
            if (input[i].equals(cierre2)) stack.pop();
            break;
          case "{":
            if (input[i].equals(cierre3)) stack.pop();
            break;
          default:
            return false;
        }
      } else if (cierre1.equals(input[i]) || cierre2.equals(input[i]) || cierre3.equals(input[i]))
        return false;

      if (stack.empty() && i == input.length - 1) return true;
    }
    return false;
  }
}
