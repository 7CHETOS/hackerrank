package com.hackerrank.java.easy;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class JavaArraylist {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        List<List<Integer>> listD = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            String str = scanner.nextLine();
            listD.add(
                    Stream.of(str.replaceAll("\\s+$", "").split(" "))
                            .map(Integer::parseInt)
                            .collect(toList()));
        }

        int q = Integer.parseInt(scanner.nextLine());

        List<List<Integer>> listQ = new ArrayList<>();

        for (int i = 0; i < q; i++) {
            String str = scanner.nextLine();
            listQ.add(
                    Stream.of(str.replaceAll("\\s+$", "").split(" "))
                            .map(Integer::parseInt)
                            .collect(toList()));
        }

        listQ.stream().forEach( (p) -> {
            int arrayLine = p.get(0) -1;
            int arrayPosition = p.get(1);

            if (arrayPosition < listD.get(arrayLine).size())
                System.out.println(listD.get(arrayLine).get(arrayPosition));
            else
                System.out.println("ERROR!");
        });

    }
}
