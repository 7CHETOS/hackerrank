package com.hackerrank.java.easy;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class JavaSubarray {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        Integer.parseInt(scanner.nextLine());
        // 5

        String b = scanner.nextLine();
        // 1 -2 4 -5 1

        List<Integer> arr;
        List<Integer> resultado = new ArrayList<>();

        arr = Stream.of(b.replaceAll("\\s+$", "").split(" "))
                            .map(Integer::parseInt)
                            .collect(toList());

        for (int i = 0; i < arr.size(); i++) {
            int temp = arr.get(i);
            resultado.add(temp);

            for (int j = i+1; j < arr.size(); j++){
                temp += arr.get(j);
                resultado.add(temp);
            }
        }

        System.out.println(resultado.stream().filter(p -> p < 0).count());

    }
}
