package com.hackerrank.java.easy;

import java.util.*;
import java.io.*;

public class JavaMap {
    public static void main(String []argh)
    {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        Map<String, Integer> map = new HashMap<String, Integer>();
        List<String> list = new ArrayList<>();

        for(int i=0;i<n;i++)
        {
            String name = scanner.nextLine();
            int phone = Integer.parseInt(scanner.nextLine());

            map.put(name, phone);
        }

        while(scanner.hasNext())
        {
            String s = scanner.nextLine();
            list.add(s);
        }

        list.stream().forEach( (l) -> {
            if (map.containsKey(l)) {
                System.out.println(l + "=" + map.get(l));
            } else {
                System.out.println("Not found");
            }
        });
    }
}
