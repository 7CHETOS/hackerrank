package com.hackerrank.java.easy;

import java.io.*;
import java.math.*;

public class JavaPrimalityTest {
    public static void main(String[] args) throws IOException {
        //BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        //
        //String n = bufferedReader.readLine();
        //
        //bufferedReader.close();

        String n = "13";
        //My code

        BigInteger bigInteger = new BigInteger(n);

        if (bigInteger.isProbablePrime(1))
            System.out.println("prime");
        else
            System.out.println("not prime");

    }
}
