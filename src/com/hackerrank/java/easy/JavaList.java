package com.hackerrank.java.easy;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class JavaList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        List<Integer> list = new ArrayList<>();

        String str = scanner.nextLine();
        list = Stream.of(str.split(" "))
                    .map(Integer::parseInt)
                    .collect(toList());

        int q = Integer.parseInt(scanner.nextLine());

        System.out.println(list);

        while (q-- > 0) {
            String query = scanner.nextLine();
            if (query.equals("Insert")) {
                String values = scanner.nextLine();
                List<Integer> listValues = new ArrayList<>();
                listValues = Stream.of(values.split(" "))
                        .map(Integer::parseInt)
                        .collect(toList());
                list.add(listValues.get(0), listValues.get(1));
            } else if (query.equals("Delete")) {
                int index = Integer.parseInt(scanner.nextLine());
                list.remove(index);
            }
        }

        System.out.println(list.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(",", "")
        );

    }
}
